<?php

namespace Model\Controller;

class Con_auto {
    
    use \doctrine\Dashes\Model;
    
    public $database = 'controller';
    public $table = 'CON_AUTO';
    public $deactivate = false;
    public $primaryKey = 'IDAUTORIZADO';

    public function autentica() {
        $codauto = $_POST['codauto'];
        $senha = md5($_POST['senha']);
        $conditions = ['CODAUTO' => $codauto, 'SENHA' => $senha];
        $dados = $this->find($conditions, 100, '', '', '');
        if(!$dados) {
            return ['status'=> 0,'msg' => 'associado nao encontrado'];
        }
        return $this->validaAssociado($dados[0]);
    }
    public function validaAssociado(&$dados) {
        if($dados['CONDICAO'] == "I") return ['status' => '0','msg' => 'acesso do associado inativado'];
        if(!empty($dados['CODPRIMARIO'])) {
            return $this->validarAutorizadoSecundarioAtivo($dados);
        }
        return ['status' => 1 , 'dados' => array_merge(['TIPO' => 'PRIMARIO'], $dados)];
    }
    
    public function validarAutorizadoSecundarioAtivo(&$dados) {
        $dados = $this->find(['CODAUTO' => $dados['CODPRIMARIO']], 1, '', '', '');
        if($dados[0]['CONDICAO'] == 'I') return ['status' => '0', 'msg' => 'acesso negado pois codigo do primario esta inativo'];
        return ['status' => 1 , 'dados' => array_merge(['TIPO' => 'SECUNDARIO'], $dados)];
    }
    public function cadastranovasenha() {
        $validacao = $this->_validacaoSenha($_POST);
        if($validacao['status']) { return $validacao; }
        $dados = [
            'IDAUTORIZADO' => $this->find(['CODAUTO' => $_POST['codcos']])[0]['IDAUTORIZADO'],
            'CODAUTO' => $_POST['codcos'],
            'SENHA' => md5($_POST['senha']),
            'FLTROCASENHA' => NULL
        ];
        $this->save($dados);
        if(isset($_POST['stoken'])) {
            $this->loadModel('\Model\Controller\Con_token');        
            $this->model['Con_token']->save(['ID' => $_POST['ID'], 'CODAUOP' => $_POST['codcos'],'STATUS' => 'USA']);
        }
        return ['status' => 1, 'msg' => 'senha alterada com sucesso'];
    }
    public  function _validacaoSenha($dados) {                
        if(strlen($dados['senha']) < 8)
            return ['status' => 1, 'msg' => 'a senha precisa conter no minimo 8 caracteres'];        
        if($dados['senha'] != $dados['confirmaSenha'])
            return ['status' => 1, 'msg' => 'houve divergencia na senha confirmada '];
    }

}