<?php

namespace Model\Controller;

class Con_altcadas {

    use \doctrine\Dashes\Model;

    public $database = 'controller';

    public function solicitaalteracaodadosprimario() {
        $this->loadModel('\Model\Protheus\YB1');
        $prospect = $this->model['YB1']->dadosProspect($_POST['CPFCNPJ']);
        if (!$prospect)
            return ['status' => 0, 'msg' => 'dados complementares nao encontrados'];
        $endereco[] = array(
            'ITEM' => 1,
            'ENDPRI' => 'S',
            'BAIRRO' => $_POST['BAIRRO'],
            'CEP' => $_POST['IDCEP'],
            'COMPLEMENTO' => $_POST['COMPLEMENT'],
            'ENDCLI' => $_POST['ENDERECO'],
            'ENDNRO' => $_POST['NUMERO'],
            'MUNICIPIO' => $_POST['NMCIDADE'],
            'TPENDERECO' => $_POST['LOGRADOURO'],
            'UF' => $_POST['IDUF'],
            'OBS' => '',
            'ROTULO' => '',
            'ENDDISTRITAL' => ' ',
        );
        $dadosProtheus = array(
            'ANEXO' => 'N',
            'TIPOOP' => '02',
            'DTALTERACAO' => date('Ymd'),
            'ENDERECO' => $endereco,
            'CARGOCONTATO' => $_POST['CARGO'],
            'CONTATO' => $_POST['CONTATO'],
            'EMAIL' => $_POST['EMAIL'],
            'EMAILCONTATO' => $_POST['EMAIL'],
            'NOMEPROSPECT' => $_POST['NOME'],
            'TELCOMERCIAL' => $_POST['TELEFONE'],
            'CELULAR' => $_POST['CELULAR'],
            'DDDCOMERCIAL' => $_POST['TELDDD'],
            'CNAE' => $prospect['dados']['YB1_CNAE'],
            'CNPJCPF' => $prospect['dados']['YB1_CGC'],
            'INSCESTADUAL' => empty($prospect['dados']['YB1_INSCR']) ? 'ISENTO' : $prospect['YB1_INSCR'],
            'DTCADASTRO' => $prospect['dados']['YB1_DATCAD'],
            'NOMEFANTASIA' => $prospect['dados']['YB1_NOME'],
            'PESSOA' => $prospect['dados']['YB1_PESSOA'],
            'DISTRITAL' => $prospect['dados']['YB1_DISTRI'],
            'TPCOBRANCA' => $prospect['dados']['YB1_X_COBR'],
            'CONDPAGAMENTO' => $prospect['dados']['YB1_CONDPG'],
            'OBSERVACAO' => $prospect['dados']['YB1_OBS'],
            'PERIODRENOVACAO' => '',
            'CLASSEASSOCIADO' => $prospect['dados']['YB1_CLASSO'],
            'IDORIGEM' => $prospect['dados']['YB1_ORIGEM'],
            'CODPROSPECT' => $prospect['dados']['YB1_CODIGO'],
            'DESCFILIACAO' => $prospect['dados']['YB1_DFILIA'],
            'NROFUNCIONARIOS' => $prospect['dados']['YB1_QTDFUN'],
            'TPFILIACAO' => $prospect['dados']['YB1_TPFILI'],
            'REPRESENTANTE' => $prospect['dados']['YB1_XREPRE'],
            'PRODREFERENCIA' => $prospect['dados']['YB1_PRDREF']
        );
        $response = $this->model['YB1']->adicionaProspect($dadosProtheus);
        if (substr($response->ADDPRIMARIORESULT, 0, 2) == '00') {
            return ['status' => 1, 'msg' => $response->ADDPRIMARIORESULT];
        }        
    }

    public function solicitainclusaoendereco() {
        $this->loadModel('\Model\Controller\Con_endrotu');
        $dadosProtheus = array(
            'BAIRRO' => $_POST['BAIRRO'],
            'CARGO' => $_POST['CARGO'],
            'CELULAR' => preg_replace('/[-\(\)]/', "", $_POST['CELULAR']),
            'CEP' => str_replace(array('.', '-'), '', $_POST['CEP']),
            'CLASSE' => $this->model['Con_endrotu']->get($_POST['ROTULO']),
            'CODCOS' => $_POST['CODCOS'],
            'COMPLEMENTO' => $_POST['COMPLEMENTO'],
            'CONTATO' => $_POST['CONTATO'],
            'DDD' => $_POST['DDD'],
            'EMAIL' => $_POST['EMAIL'],
            'ENDERECO' => $_POST['ENDERECO'],
            'MUNICIPIO' => $_POST['MUNICIPIO'],
            'NROSOLIC' => 28, // insert id da tabela CON_ALTENDE - buscar na tabela a ultima solicitacao ????
            'NUMERO' => $_POST['NUMERO'],
            'ROTULO' => $_POST['ROTULO'],
            'TELEFONE' => preg_replace('/[-\(\)]/', "", $_POST['TELEFONE']),
            'TIPOOP' => $_POST['TIPOOP'], // 01 - inclusao, 02 - alteracao
            'TPENDERECO' => $_POST['TPENDERECO'],
            'UF' => $_POST['UF'],
            'SEQ' => @$_POST['SEQUENCIA'], //na inclusao, enviar em branco, na alteracao enviar o campo PA1_CODIGO
            'ENDDISTRITAL' => ' ',
        );
        $this->loadModel('\Model\Protheus\YB1');
        $response = $this->model['YB1']->addSolicEndereco($dadosProtheus); 
        if (substr($response->ADDENDERECORESULT, 0, 2) == '00') {
            return ['status' => 1, 'msg' => $response->ADDENDERECORESULT];
        }
        return ['status' => 0, 'msg' => $response->ADDENDERECORESULT];
    }
}
