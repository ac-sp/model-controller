<?php

namespace Model\Controller;

class Con_endrotu {
    
    use \doctrine\Dashes\Model;
    
    public $database = 'controller';
    public $table = 'CON_ENDROTU';
    public $deactivate = false;

    public function get($id = null) {
        $result = $this->find(["ID_ENDROTU" => $id],1,'','');
        return $result[0]['NOME'];
    }
}