<?php

namespace Model\Controller;

class Con_marpress {
    
    use \doctrine\Dashes\Model;
    
    public $database = 'controller';
    public $table = 'CON_MARPRESS';
    public $deactivate = false;
    public $primaryKey = 'IDAUTORIZADO';

    public function demonstrativo($prefixo, $numero, $parcela) {
        $conditions = ['PREFIXO' => $prefixo, 'NUMERO' => $numero, 'PARCELA' => @$parcela ? $parcela : ' '];
        $dados = $this->find($conditions, 1, '', '', '');
        return $dados ? ['status' => 1, 'dados' => $dados] : ['status' => 0, 'msg' => 'dados nao encontrados'];
    }
    
    public function dadosboleto($titulo) {
        
    }
    public function boletos($item) {
        
    }
    /*
     * funcoes do controller
     */
    /*
     public function boleto($id = NULL)
    {
        $this->load->model('Marpress_model', 'mdlMarpress');

        if (NULL === $id) {
            $this->load->model('Associado_model', 'mdlAssociado');
            $sess_data = $this->session->userdata('ASSOCIADO');
            
            $associado = $this->mdlAssociado->getById($sess_data['CODAUTO']);
            
//            if ($associado['CLITPCOB'] == 'E1') {
//                $dados['mensagem'] = 'Você recebe o boleto via correio, se quiser receber via web, entre em contato com (011) 3180-3737.';
//                
//            } else {
//                $dados['dados'] = $this->mdlMarpress->getFaturas($sess_data['CODAUTO']);
//            }
            $dados['dados'] = $this->mdlMarpress->getFaturas($sess_data['CODAUTO']);
            $this->load->view('boleto', $dados);
            
        } else {
            disableLayout();

            $fatura = $this->mdlMarpress->getById(base64_decode($id));

            $vencimento = Formata::dataBoleto($fatura[0]->VENCIMENTO);

            $data = array(
                'dados_cliente' => array(
                    'sacado'    => $fatura[0]->RAZAO,
                    'endereco1' => $fatura[0]->ENDERECO,
                    'endereco2' => "{$fatura[0]->BAIRRO} - {$fatura[0]->MUNICIPIO} - {$fatura[0]->UF} - CEP: {$fatura[0]->CEP}",
                ),
                'dados_empresa' => array(
                    'agencia'       => $fatura[0]->AGENCIA,
                    'agencia_dv'    => $fatura[0]->AGENCIADIG,
                    'conta'         => $fatura[0]->CONTA,
                    'conta_dv'      => $fatura[0]->CONTADIG,
                    'carteira'      => substr($fatura[0]->CARTEIRA, -2)
                ),
                'dados_boleto' => array(
                    'data_vencimento'   => $vencimento,
                    'nosso_numero'      => substr(substr($fatura[0]->NOSSONUM, -12), 0, 11),
                    'numero'            => "{$fatura[0]->PREFIXO}{$fatura[0]->NUMERO}{$fatura[0]->PARCELA}",
                    'info_produtos'     => $fatura[0]->INFOPRODUTOS,
                    'informativo'       => $fatura[0]->INFORMATIVO
                ),
                'valores_boleto'    => array(
                    'valor_cobrado' => $fatura[0]->VALORSEMIMP,
                    'especie'       => $fatura[0]->TPMOEDA,
                    'mora_dia'      => $fatura[0]->MORADIA
                )
            );

            $this->load->helper('My_boleto_bradesco');
            boleto_bradesco(
                $data['dados_cliente'], 
                $data['dados_empresa'], 
                $data['dados_boleto'], 
                $data['valores_boleto']
            );
        }
    }

    
     * Tela para exibi��o do demonstrativo
     *
     * @return void
    
    public function demonstrativo($prefixo, $numero, $parcela = null) {
        disableLayout();
        $this->load->model('Marpress_model');
        $sess_data = $this->session->userdata('ASSOCIADO');
        $fatura = $this->Marpress_model->getFatura($sess_data['CODAUTO'], $prefixo, $numero, $parcela);
        $this->load->view('demonstrativo', array('dados' => $fatura));
    }
    */
}