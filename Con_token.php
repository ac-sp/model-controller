<?php

namespace Model\Controller;

class Con_token {
    
    use \doctrine\Dashes\Model;
    
    public $database = 'controller';
    public $table = 'CON_TOKEN';
    public $deactivate = false;
    public $primaryKey = 'ID';
    

    public function redefinicaoSenha() {
        $this->loadModel('\Model\Protheus\SA1');        
        $result = $this->model['SA1']->dadosassociado($_POST['codcos']);        
        if(!$result['status']) return ['status' => 0, 'msg' => 'associado nao encontrado'];
        $item = [
            'TOKEN' => md5($result['dados']['A1_CODCOS'].$result['dados']['A1_CGC'].date('Y-m-d H:i:s')),
            'CODAUOP' => $result['dados']['A1_CODCOS'],
            'DATA' => date('Y-m-d'),
            'HORA' => date('H:i:s'),
            'STATUS' => 'NOV'
        ];
        $this->save($item);
        return ['status' => 1, 'msg' => 'redefinicao de senha cadastrada'];
    }
    public function getToken($token) {
        $dados = $this->find(['TOKEN' => $token, 'STATUS' => 'NOV']);        
        return $dados ? ['status' => 1, 'dados' => $dados] : ['status' => 0, 'msg' => 'dados nao encontrados'];
    }
    public function sendEmail($token, $email) {
        $url_token = "http://dcontrollerapi.acsp.com.br/api/associado/novaSenha?token={$token}"; 
        
        $mailSets = \Crush\Collection::filter($this->config->config, function ($val, $key) {
            return in_array($key, ['protocol', 'charset', 'smtp_host', 'smtp_port', 'smtp_user', 'smtp_pass', 'mailtype', 'validation', 'address_to', 'address_cc', 'address_bcc'], true);
        });
        $data['address_to'] = 'marcio.pereira@fcamara.com.br';
        $mailSets['host'] = $mailSets['smtp_host'];
        $mailSets['user'] = $mailSets['smtp_user'];
        $mailSets['pass'] = $mailSets['smtp_pass'];
        $mailSets['port'] = $mailSets['smtp_port'];
        
        $data['body'] = "<p>Mensagem : {$url_token}</p>";
        $mailSets['address_to'] = !empty(@$data['address_to']) ? $data['address_to'] : (!empty($mailSets['address_to']) ? $mailSets['address_to'] : $mailSets['smtp_user']);
        
        $result = \Crush\Email::sendSMTP($data, $mailSets);
        die(json_encode($result));
    }
}
